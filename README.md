# Exchange Rate Service

In order for a Bookmaker to see their liabilities in a common currency, An exchange rate microservice is required. This exchange rate service will be responsible for collection and delivery of exchange rate values to internal clients. 

#### It must: ####

- Provide a RESTful api to fetch an exchange rate for a specified _base currency_ and _target currency_;
- Store the given exchange rate in a database, timestamped with audit trail;
- The most recent exchange rate for the requested currency exchange should be returned;
- Converstion rates are to be stored to 5 decimal places;
- Any design decisions and assumptions should be documented.


#### Implementation: ####
The solution must conform to the below requirements:

- C#/ Visiual Studio 2015+;
- Entity Framework 6.x+ / Code First Migrations;
- Leverage a DI Container of your choice;
- Utilize open-source/ free nuget packages/ extensions;
- Provide a RESTful endpoint as described below;
- Retrieve exchange rates from https://fixer.io/


#### Data Source ####
your solution must acquire conversion rates via the REST api provided by fixer.io

all non-matching pairs of _base_ and _target_ currency of the following currencies should be imported:

-        AUD;
-        SEK;
-        USD; 
-        GBP;
-        EUR.


#### REST Api: ####

##### Retrieve Exchange Rate #####

The api must support a GET HTTP verb with the following contract:



```json

{
    "baseCurrency": "GBP",
    "targetCurrency": "AUD"
}

```
where

- baseCurrency : string, required;
- targetCurrency : string, required;


with response:



```json

{
    "baseCurrency": "GBP",
    "targetCurrency": "AUD",
    "exchangeRate" : 0.7192,
    "timestamp" : "2018-03-21T12:13:48.00Z"
}

```

#### NFR's ####

- Expected load on this API is ~5 - 15 requests/ sec;
- Deployed to a load-balanced environment (~2-3 nodes).


#### Submission ####

For a submission to be considered complete it **must**:

 - Demonstrate appropriate use of source control/ versioning;
 - Adhere to modern coding standards/ practices;
 - Be merged to master;
 - Be testable;
 - Solve the problem.

This repository should be forked and shared with _techtest_au@kindredgroup.com_ when complete.

