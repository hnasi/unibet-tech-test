﻿using CurrencyRate.Services;
using System;
using Xunit;
using Moq;
using CurrencyRate.Models;
using CurrencyRate.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace CurrencyRate.Tests.Controllers
{
    public class CurrencyControllerTests
    {
        [Fact]
        public void Returns_ValidResponse()
        {
            // Arrange
            var service = new Mock<IForexService>(); ;
            var timestamp = DateTime.Now;

            service.Setup(c => c.GetExchangeRate(It.IsAny<string>(), It.IsAny<string>())).Returns(new CurrencyRateResponse
            {
                BaseCurrency = "AUD",
                TargetCurrency = "USD",
                ExchangeRate = (decimal)1.23456,
                Timestamp = timestamp
            });

            var controller = new CurrencyController(service.Object);

            // Act
            var result = controller.Get("AUD", "USD");

            // Assert
            Assert.IsType<OkObjectResult>(result);
            var rateResponse = (((OkObjectResult)result).Value as CurrencyRateResponse);
            Assert.Equal("AUD", rateResponse.BaseCurrency);
            Assert.Equal("USD", rateResponse.TargetCurrency);
            Assert.Equal((decimal)1.23456, rateResponse.ExchangeRate);
            Assert.Equal(timestamp, rateResponse.Timestamp);
        }
    }
}
